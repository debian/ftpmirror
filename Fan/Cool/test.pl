BEGIN {
	$| = 1;
	print("1..2\n");
}
END {
	print("not ok 1\n") unless $loaded;
}

use Fan::Cool;
use Fan::Usage;
# $LOG = 7;
$loaded = 1;
print("ok 1\n");

# 100000 -> 260K
# 200000 -> 464K
# 400000 -> 836K
$string = "nan-jara-hoi";
&put_string(1, $string);
printf("* before maxrss = %d\n", &get_maxrss);
&put_string(500000, $string);
printf("* result maxrss = %d\n", &get_maxrss);

sub put_string ($$) {
	my $n = shift;
	my $zz = shift;

	while ($n-- > 0) {
		plog(7, "hogehoge, hogehoge, is $zz\n");
#		&dummy(7, "hogehoge, hogehoge, is $zz\n");
	}
}

sub get_maxrss {
	my $u = getrusage;
	$u->ru_maxrss;
}

sub dummy {
	my $level = shift;

	if ($level > 0) {
		grep(1, @_);
	}
}
